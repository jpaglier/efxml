# Creates an XML summary of the header of all mail messages in a
# mailbox in mbox format.  The XML summary conforms to mbox-summary.dtd

# Usage: python create-mbox-summary.py <mbox mailbox file>
# Output: XML summary printed to stdout

#     Copyright 2006 Philip J. Guo
#     http://alum.mit.edu/www/pgbovine/

# Created: 2006-08-25

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

import sys
import re
from mailbox import PortableUnixMailbox
from email.Utils import parsedate
from datetime import date

# Look for stuff like "Real name <email-addr@domain.com>"
quoted_addr = re.compile('<.*@.*>')

more_than_one_at = re.compile('.*@.*@')

from_line = re.compile('^From .*@.*')

# PortableUnixMailbox is the .mbox format that considers regexp "^From"
# as the only delimiter between messages
mail_fp = open(sys.argv[1], 'rb')
mailbox = PortableUnixMailbox(mail_fp)

print '<?xml version="1.0" encoding="ISO-8859-1"?>'
print '<!DOCTYPE gallery SYSTEM "mbox-summary.dtd">'
print
print "<mailbox>"
print

msg_end = 0  # end of last message, init to 0 for no reason -justin

for msg in mailbox:
    # this header begins immediately after the last message ends
    header_begin = msg_end

    # If the message does not contain a valid 'From' header,
    # then it's damaged goods and we should punt and skip it:
    if ('From' not in msg) or (msg['From'].strip() == ""):
        if 'Date' in msg:
            sys.stderr.write('Error: Message sent on ' + msg['Date'] + ' is faulty!!!\n')
        else:
            sys.stderr.write('Error: Message is faulty!!!\nmsg: ' + str(msg) + "\noffset: " + str(mail_fp.tell()) + '\n')
        continue

    
    print "<message>"
    
    # THIS LINE PRINTS THE BEGINNING BYTE OF A MESSAGE
    # print "<byte>" + str(mail_fp.tell())
    
    if "Subject" in msg:
        print "<Subject><![CDATA[" + msg['Subject'] + "]]></Subject>"

    if "Date" in msg:
        date_str = msg['Date']
        parsed_date = parsedate(date_str)
        print "<Date>"
        tag_names = ['year', 'month', 'day', 'hour', 'min', 'sec']
        i = 0
        for t in tag_names:
            print '<' + t + '>' + str(parsed_date[i]) + '</' + t + '>'
            i += 1
        print "<date_str><![CDATA[" + date_str + "]]></date_str>"
        print "</Date>"

    # Grab all recipients from To, Cc, and Bcc:
    # Add Return-Path and Delivered-To --justin
    for header in ['From', 'To', 'Cc', 'Bcc', 'Return-Path', 'Delivered-To']:
        if header not in msg:
            continue

        recipients_str = msg[header]

        recipients_str = recipients_str.strip()

        # If it's an empty string, then punt!
        if len(recipients_str) == 0:
            continue

        print "<" + header + ">"

        # Ok, this is friggin' annoying.  The address list is
        # delimited by commas, but there may be commas within quotes,
        # which should be substituted.  e.g.:
        #    To: "Smith, Adam" <ASmith@econ.com>
        #
        # Same goes for '<' and '>' because other tokenizers use them.
        #
        # Replace commas inside of double quotes with a weird thing
        # "&^%$#", to re replaced back later, left bracket with
        # "&lt;", and right bracket with "&gt;"
        real_recipients = []
        inside_double_quote = False
        for c in recipients_str:
            replace_comma = False
            replace_lt = False
            replace_gt = False

            if c == '"':
                # Invert
                inside_double_quote = not inside_double_quote

            if inside_double_quote:
                if c == ',':
                    replace_comma = True
                elif c == '<':
                    replace_lt = True
                elif c == '>':
                    replace_gt = True

            if replace_comma:
                real_recipients.append('&^%$#')
            elif replace_lt:
                real_recipients.append('&lt;')
            elif replace_gt:
                real_recipients.append('&gt;')
            else:
                real_recipients.append(c)

        # Gypsy switch
        recipients_str = ''.join(real_recipients)

        # Tokenize based on ','
        recipients_lst = recipients_str.split(',')
        for r in recipients_lst:
            email = None
            alias = None

            # Pathological case: "To:  <John.Doe@anonymous.com> <John.Doe@anonymous.com>"
            # We only want to match on the LAST '@'
            if more_than_one_at.search(r):
                # Split on space and grab the LAST ENTRY
                poop = r.split()
                r = poop[-1]
                alias = ' '.join(poop[:-1])

            m = quoted_addr.search(r)
            if m:
                email = m.group(0)[1:-1]
                # The alias is EVERYTHING before the '<':
                alias = r.split('<')[0]

            # Otherwise assume the whole thing is an address, and there is no alias
            elif '@' in r:
                email = r.strip()
            # If you don't even see a '@', and the string is
            # non-empty, then you've gotta assume that it's simply a
            # name without an email address
            else:
                r_s = r.strip()
                if (len(r_s) > 0):
                    alias = r_s

            # If you're totally blank, then punt
            if not alias and not email:
                continue

            print "<recipient>"
            
            if alias:
                alias = alias.strip()
                if len(alias) > 0:
                    alias = alias.replace('&^%$#', ',').replace('&lt;', '<').replace('&gt;', '>')
                    # Strip off double quotes if they surround alias
                    if alias[0] == '"' and alias[-1] == '"':
                        alias = alias[1:-1]
                    print "<alias><![CDATA[" + alias + "]]></alias>"

            if email:
                print "<email>" + email + "</email>"

            print "</recipient>"

        print "</" + header + ">"
    
    # print msg.getallmatchingheaders('Received')
    
    past_first_block = False;  # this is used in a horrible hack to organize received blocks
    if 'Received' in msg:
        for received in msg.getallmatchingheaders('Received'):
            # clean up, remove received: , and split
            if received.find("Received: ") == 0:  # beginning of the past_first_block
                if past_first_block:  # if you need to end a previous past_first_block
                    print "</Received>"
                print "<Received>"  # open a new block 
                received = received.lstrip("Received: ")  # clean off the cruft
                past_first_block = True  # set a flag
                
            # escape special characters and split at whitespace
            received = received.strip().replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;").replace('"', "&quot;").split()
            if received[0] in ['by', 'for', 'with', 'id', "from"]:
                print "<" + received[0] + ">"
                print " ".join(received[1:])
                print "</" + received[0] + ">"
            else:
                print " ".join(received)
            
    # if past_first_block:
        # close the final block
        print "</Received>"
    
    # find the last line
    # if we wanted to do body processing we could do it here
    # -justin
    line = mail_fp.readline()
    while not from_line.match(line) and line:
        # last_line = line
        line = mail_fp.readline()
    msg_end = mail_fp.tell() - len(line)
    #####################
    # add byte runs tag  #
    # fs_offset          #
    # img_offset         #
    # not implemented yet#
    # :):) -justin       #
    #####################
    print "<byte_runs>"
    print "<run fs_offset='not_implemented' img_offset='not_implemented' file_offset='%d' len='%d'/>" % (header_begin, msg_end - header_begin)
    print "</byte_runs>"
    ####################
    print "</message>"
    print
print "</mailbox>"
