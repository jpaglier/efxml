# Creates an XML summary of the header of all mail messages in a
# mailbox in mbox format.  The XML summary conforms to mbox-summary.dtd

# Usage: python create-mbox-summary.py <mbox mailbox file>
# Output: XML summary printed to stdout

#     Copyright 2006 Philip J. Guo
#     http://alum.mit.edu/www/pgbovine/

# Created: 2006-08-25

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.


'''
TODO: Internationalization isn't handled very well. For example, the ANSI character \xf8 neededs to be
decoded from latin-1, but utf-8 won't handle it. This is a known deficiency and could be corrected in future
work.

-jpaglier
'''


import sys
import os
import re
from mailbox import PortableUnixMailbox
from email.Utils import parsedate
from datetime import date
from lxml import etree
import hashlib

def process(mbox_dir, output_dir):       
    efxml = etree.Element('mailbox')
    efxml.set('type', 'mbox')
    tree = etree.ElementTree(efxml)
    for mbox_file in os.listdir(mbox_dir):
        process_inner(mbox_dir, mbox_file, efxml)

    #writes to stdout
    tree.write("{0}\\{1}.efxml".format(output_dir, os.path.basename(mbox_dir)), encoding='ISO-8859-1', xml_declaration=True, pretty_print=True)

def process_inner(mbox_dir, mbox_file, efxml):
    # Look for stuff like "Real name <email-addr@domain.com>"
    quoted_addr = re.compile('<.*@.*>')
    
    more_than_one_at = re.compile('.*@.*@')
    
    from_line = re.compile('^From .*@.*')
    
    # PortableUnixMailbox is the .mbox format that considers regexp "^From"
    # as the only delimiter between messages
    mail_fp = open("{0}\\{1}".format(mbox_dir, mbox_file), 'rb')
    mailbox = PortableUnixMailbox(mail_fp)
    
    msg_end = 0  # end of last message, init to 0 for no reason -justin
    
    folder = etree.SubElement(efxml, 'folder')
    folder.set('name', mbox_file)
    
    for msg in mailbox:
        # this header begins immediately after the last message ends
        header_begin = msg_end
    
        # If the message does not contain a valid 'From' header,
        # then it's damaged goods and we should punt and skip it:
        if ('From' not in msg) or (msg['From'].strip() == ""):
            if 'Date' in msg:
                sys.stderr.write('Error: Message sent on ' + msg['Date'] + ' is faulty!!!\n')
            else:
                sys.stderr.write('Error: Message is faulty!!!\nmsg: ' + str(msg) + "\noffset: " + str(mail_fp.tell()) + '\n')
            continue
    
        message = etree.SubElement(folder, 'message')
        
        # THIS LINE PRINTS THE BEGINNING BYTE OF A MESSAGE - test info
        # print "<byte>" + str(mail_fp.tell())
        
        for header in ['Message-ID', 'Resent-Message-ID', 'Original-Message-ID']:
            if header in msg:
                message_id = etree.SubElement(message, header)
                message_id.text = msg[header]
        
        if "Subject" in msg:
            subject = etree.SubElement(message, 'Subject')
            subject.text = "<![CDATA[" + msg['Subject'].decode("latin-1") + "]]>" 
    
        for header in ['Date', "Resent-Date", 'Delivery-Date', 'Expiry-Date', 'Expires', 'Reply-By', 'Latest-Delivery-Time']:
            if header in msg:
                date_str = msg[header]
                parsed_date = parsedate(date_str)
                date = etree.SubElement(message, header)
                tag_names = ['year', 'month', 'day', 'hour', 'min', 'sec']
                i = 0
                for t in tag_names:
                    etree.SubElement(date, t).text = str(parsed_date[i])
                    i += 1
                etree.SubElement(date, 'date_str').text = "<![CDATA[" + date_str.decode("latin-1") + "]]>"
    
        #Grab all senders: From, Sender, Reply-To, Return-path, etc
        #-justin
        for header in ['From', 'Sender', 'Reply-To', 'Return-Path', 'Resent-From', 'Resent-Sender', "Resent-Reply-To", 'Originator-Return-Address']:
            if header not in msg:
                continue
            
            sender_str = msg[header]
            sender_str = sender_str.strip()
            if len(sender_str) == 0:
                continue
            header_sub = etree.SubElement(message, header)
            # Tokenize based on ','
            sender_lst = sender_str.split(',')
            for r in sender_lst:
                email = None
                alias = None
    
                # Pathological case: "To:  <John.Doe@anonymous.com> <John.Doe@anonymous.com>"
                # We only want to match on the LAST '@'
                if more_than_one_at.search(r):
                    # Split on space and grab the LAST ENTRY
                    patho = r.split()
                    r = patho[-1]
                    alias = ' '.join(patho[:-1])
    
                m = quoted_addr.search(r)
                if m:
                    email = m.group(0)[1:-1]
                    # The alias is EVERYTHING before the '<':
                    alias = r.split('<')[0]
    
                # Otherwise assume the whole thing is an address, and there is no alias
                elif '@' in r:
                    email = r.strip()
                # If you don't even see a '@', and the string is
                # non-empty, then you've gotta assume that it's simply a
                # name without an email address
                else:
                    r_s = r.strip()
                    if (len(r_s) > 0):
                        alias = r_s
    
                # If you're totally blank, then punt
                if not alias and not email:
                    continue
    
                sender = etree.SubElement(header_sub, 'sender')
                
                if alias:
                    alias = alias.strip()
                    if len(alias) > 0:
                        alias = alias.replace('&^%$#', ',').replace('&lt;', '<').replace('&gt;', '>')
                        # Strip off double quotes if they surround alias
                        if alias[0] == '"' and alias[-1] == '"':
                            alias = alias[1:-1]
                        etree.SubElement(sender, 'alias'). text = "<![CDATA[" + alias.decode("latin-1") + "]]>"
    
                if email:
                    etree.SubElement(sender, 'email').text = email
            
        
        #for header in ['From', 'To', 'Cc', 'Bcc', 'Return-Path', 'Delivered-To']:
        for header in ['To', 'Cc', 'Bcc', 'Delivered-To', 'Resent-To', 'Resent-Cc', 'Resent-Bcc']:
            if header not in msg:
                continue
    
            recipients_str = msg[header]
    
            recipients_str = recipients_str.strip()
    
            # If it's an empty string, then punt!
            if len(recipients_str) == 0:
                continue
    
            header_sub = etree.SubElement(message, header)
    
            # Tokenize based on ','
            recipients_lst = recipients_str.split(',')
            for r in recipients_lst:
                email = None
                alias = None
    
                # Pathological case: "To:  <John.Doe@anonymous.com> <John.Doe@anonymous.com>"
                # We only want to match on the LAST '@'
                if more_than_one_at.search(r):
                    # Split on space and grab the LAST ENTRY
                    patho = r.split()
                    r = patho[-1]
                    alias = ' '.join(patho[:-1])
    
                m = quoted_addr.search(r)
                if m:
                    email = m.group(0)[1:-1]
                    # The alias is EVERYTHING before the '<':
                    alias = r.split('<')[0]
    
                # Otherwise assume the whole thing is an address, and there is no alias
                elif '@' in r:
                    email = r.strip()
                # If you don't even see a '@', and the string is
                # non-empty, then you've gotta assume that it's simply a
                # name without an email address
                else:
                    r_s = r.strip()
                    if (len(r_s) > 0):
                        alias = r_s
    
                # If you're totally blank, then punt
                if not alias and not email:
                    continue
    
                recipient = etree.SubElement(header_sub, 'recipient')
                
                if alias:
                    alias = alias.strip()
                    if len(alias) > 0:
                        alias = alias.replace('&^%$#', ',').replace('&lt;', '<').replace('&gt;', '>')
                        # Strip off double quotes if they surround alias
                        if alias[0] == '"' and alias[-1] == '"':
                            alias = alias[1:-1]
                        etree.SubElement(recipient, 'alias'). text = "<![CDATA[" + alias.decode("latin-1") + "]]>"
    
                if email:
                    etree.SubElement(recipient, 'email').text = email
        
        if 'In-Reply-To' in msg:
            irt = etree.SubElement(message, 'In-Reply-To')
            irt.text = msg['In-Reply-To']
            
        if 'References' in msg:
            ref_elem = etree.SubElement(message, 'References')
            
            ref_str = msg['References']
            for ref in ref_str.split():
                ref_subelem = etree.SubElement(ref_elem, 'ref')
                ref_subelem.text = ref
                
        if 'Comments' in msg:
            comments = etree.SubElement(message, "Comments")
            comments.text = msg['Comments']
    
        if 'Keywords' in msg:
            keywords_elem = etree.SubElement(message, "Keywords")
            keywords_str = msg['Keywords']
            for keyword in keywords_str.split(","):
                keyword_elem = etree.SubElement(keywords_elem, "word")
                keyword_elem.text = keyword
        
        if 'Received' in msg:
            for received in msg.getallmatchingheaders('Received'):
                received_node = etree.SubElement(message, 'Received') #create node
                received = received.replace("Received: ", "", 1) #clean up
                for line in received.strip().split("\n"): #line by line
                    #print line
                    line = line.decode("latin-1")
                    line = line.strip() #clean up
                    split_line = line.split(" ", 1)
                    #print split_line
                    if(split_line[0] in ['from', 'by', 'via', 'with', 'id', 'for']):
                        etree.SubElement(received_node, split_line[0]).text = split_line[1]
                    else:
                        etree.SubElement(received_node, 'other').text = line
                        
        #this is apparently deprecated but we'll dump it in just in case its there
        if 'Encrypted' in msg:
            enc_sub = etree.SubElement(message, "Encrypted")
            enc_sub.text = msg['Encrypted']
         
        #couldnt really find examples of these Disposition-Notification ones...   
        if 'Disposition-Notification-To' in msg:
            dnt_sub = etree.SubElement(message, "Disposition-Notification-To")
            dnt_sub.text = msg['Disposition-Notification-To']        
        
        if 'Disposition-Notification-Options' in msg:
            dnt_sub = etree.SubElement(message, "Disposition-Notification-Options")
            dnt_sub.text = msg['Disposition-Notification-Options']   
        
        #this is standard, though non-standard examples show up from time to time like X-Accept-Lanuage
        #we're just gonna ignore those for the PoC -justin
        for header in ['Accept-Language', 'Language']:
            if header in msg:
                al_parent = etree.SubElement(message, header)
                al_str = msg[header]
                for lang in al_str.split():
                    lang_elem = etree.SubElement(al_parent, 'lang')
                    lang_elem.text = lang
                
        #another one with no examples found...
        if 'PICS-Label' in msg:
            pl_parent = etree.SubElement(message, "PICS-Label")
            pl_parent.text = msg['PICS-Label']
            
        if 'Encoding' in msg:
            encoding_parent = etree.SubElement(message, 'Encoding')
            encoding_parent.text = msg['Encoding']
                
        list_parent = etree.SubElement(message, "List")
        for header in ['List-Archive', 'List-Help', 'List-ID', 'List-Owner', 'List-Post', 'List-Subscribe', 'List-Unsubscribe']:
            if header in msg:
                list_node = etree.SubElement(list_parent, header)
                list_node.text = msg[header]
        
        if 'Message-Context' in msg:
            context = etree.SubElement(message, 'Message-Context')
            context.text = msg['Message-Context']
        
        #cant find examples of the next few...listed as strange in the RFC
        if 'DL-Expansion-History' in msg:
            dleh = etree.SubElement(message, 'DL-Expansion-History')
            dleh.text = msg['DL-Expansion-History']
        
        if 'Alternate-Recipient' in msg:
            altr = etree.SubElement(message, 'Alternate-Recipient')
            altr.text = msg['Alternate-Recipient']
        
        if 'Original-Encoded-Information-Types' in msg:
            oeit = etree.SubElement(message, 'Original-Encoded-Information-Types')
            oeit.text = msg['Original-Encoded-Information-Types']
            
        if 'Content-Return' in msg:
            cr = etree.SubElement(message, 'Content-Return')
            cr.text = msg['Content-Return']
            
        if 'Generate-Delivery-Report' in msg:
            gdr = etree.SubElement(message, 'Generate-Delivery-Report')
            gdr.text = msg['Generate-Delivery-Report']    
        
        if 'Prevent-NonDelivery-Report' in msg:
            pndr = etree.SubElement(message, 'Prevent-NonDelivery-Report')
            pndr.text = msg['Prevent-NonDelivery-Report']
        
        #these two are essentially the same
        for header in ['Obsoletes', 'Supersedes']:
            if header in msg:
                node = etree.SubElement(message, header)
                node.text = msg[header]
                
        if 'Content-Identifier' in msg:
            node = etree.SubElement(message, 'Content-Identifier')
            node.text = msg['Content-Identifier']
            
        if 'Importance' in msg:
            node = etree.SubElement(message, 'Importance')
            node.text = msg['Importance']
        
        if 'Incomplete-Copy' in msg:
            node = etree.SubElement(message, 'Incomplete-Copy')
            node.text = msg['Incomplete-Copy']
        
        if 'Priority' in msg:
            node = etree.SubElement(message, 'Priority')
            node.text = msg['Priority']
            
        if 'Sensitivity' in msg:
            node = etree.SubElement(message, 'Sensitivity')
            node.text = msg['Sensitivity']
        
        if 'Conversion' in msg:
            node = etree.SubElement(message, 'Conversion')
            node.text = msg['Conversion']
            
        if 'Conversion-With-Loss' in msg:
            node = etree.SubElement(message, 'Conversion-With-Loss')
            node.text = msg['Conversion-With-Loss']
            
        if 'Message-Type' in msg:
            node = etree.SubElement(message, 'Message-Type')
            node.text = msg['Message-Type']
            
        for header in ['Autosubmitted', 'Autoforwarded']:
            if header in msg:
                node = etree.SubElement(message, header)
                node.text = msg[header]
        
        ###################################################################################
        #just as a note most of these if statements are listed as "not for general use"...#
        ###################################################################################
        
        if 'Discarded-X400-IMPS-Extensions' in msg:
            node = etree.SubElement(message, 'Discarded-X400-IMPS-Extensions')
            node.text = msg['Discarded-X400-IMPS-Extensions']
        
        if 'Discarded-X400-MTS-Extensions' in msg:
            node = etree.SubElement(message, 'Discarded-X400-MTS-Extensions')
            node.text = msg['Discarded-X400-MTS-Extensions']
            
        if 'Disclose-Recipients' in msg:
            node = etree.SubElement(message, 'Disclose-Recipients')
            node.text = msg['Disclose-Recipients']
            
        if 'Deferred-Delivery' in msg:
            node = etree.SubElement(message, 'Deferred-Delivery')
            node.text = msg['Deferred-Delivery']
            
        #a bunch more not for general use...grouped together into thsi loop by name
        for header in ['X400-Content-Identifier', 'X400-Content-Return', 'X400-Content-Type', 'X400-MTS-Identifier', 'X400-Originator', 'X400-Received', 'X400-Recipients', 'X400-Trace']:
            if header in msg:
                node = etree.SubElement(message, header)
                node.text = msg[header]
    
    ##############
    #MIME headers#
    ##############
        if 'MIME-Version' in msg:
            node = etree.SubElement(message, 'MIME-Version')
            node.text = msg['MIME-Version']
        
        if 'Content-ID' in msg:
            node = etree.SubElement(message, 'Content-ID')
            node.text = msg['Content-ID']
            
        if 'Content-Description' in msg:
            node = etree.SubElement(message, 'Content-Description')
            node.text = msg['Content-Description']
            
        if 'Content-Transfer-Encoding' in msg:
            node = etree.SubElement(message, 'Content-Transfer-Encoding')
            node.text = msg['Content-Transfer-Encoding']
            
        if 'Content-Type' in msg:
            node = etree.SubElement(message, 'Content-Type')
            node.text = msg['Content-Type']    
    
        if 'Content-Base' in msg:
            node = etree.SubElement(message, 'Content-Base')
            node.text = msg['Content-Base']
            
        if 'Content-Location' in msg:
            node = etree.SubElement(message, 'Content-Location')
            node.text = msg['Content-Location']
        
        if 'Content-features' in msg:
            node = etree.SubElement(message, 'Content-features')
            node.text = msg['Content-features']
            
        if 'Content-Disposition' in msg:
            node = etree.SubElement(message, 'Content-Disposition')
            node.text = msg['Content-Disposition']
            
        if 'Content-Language' in msg:
            node = etree.SubElement(message, 'Content-Language')
            node.text = msg['Content-Language']
            
        if 'Content-Alternative' in msg:
            node = etree.SubElement(message, 'Content-Alternative')
            node.text = msg['Content-Alternative']
            
        if 'Content-MD5' in msg:
            node = etree.SubElement(message, 'Content-MD5')
            node.text = msg['Content-MD5']
        
        if 'Content-Duration' in msg:
            node = etree.SubElement(message, 'Content-Duration')
            node.text = msg['Content-Duration']
        
        #all of those X- headers that aren't in the RFC....
        nonstandard = None
        for header in msg.headers:
            if header[0:2] == "X-":
                if nonstandard == None:
                    nonstandard = etree.SubElement(message, "Not-In-RFC")
                nsnode = etree.SubElement(nonstandard, header.split(':')[0])
                try:
                    nsnode.text = msg[header.split(':')[0]]
                except ValueError:
                    nsnode.text = "Incompatible characters"
        
        # find the last line
        # if we wanted to do body processing we could do it here
        # -justin
        line = mail_fp.readline()
        while not from_line.match(line) and line:
            # last_line = line
            line = mail_fp.readline()
        msg_end = mail_fp.tell() - len(line)
        #####################
        # add byte runs tag  #
        # fs_offset          #
        # img_offset         #
        # not needed here    #
        # :):) -justin       #
        #####################
        byte_runs = etree.SubElement(message, 'byte_runs')
        #we dont use these - dont make sense in context
        #
        #byte_runs.set('fs_offset', 'not_implemented')
        #byte_runs.set('img_offset', 'not_implemented')
        #
        byte_runs.set('file_offset', str(header_begin))
        byte_runs.set('len', str(msg_end - header_begin -2)) #hack off the last 2 bytes to avoid a newline
    
        #calulate a checksum of the message -justin
        mail_fp.seek(header_begin)
        wholemsg = mail_fp.read(msg_end - header_begin - 2) #grab the whole message for hashing
        checksum = etree.SubElement(message, "checksum")
        #could add more hash algos here if desired - just add another sub elem and digest it
        md5 = etree.SubElement(checksum, 'md5')
        md5.text = hashlib.md5(wholemsg).hexdigest()
        sha1 = etree.SubElement(checksum, "sha1")
        sha1.text = hashlib.sha1(wholemsg).hexdigest()
    
if __name__ == '__main__':
    #this is hackish but itll work for what we're doing
    #itll only go two directories deep here - the assumption is that
    #you have a dir that contains many dirs that contain the output from readpst
    output_dir = sys.argv[1] + "\\outputs"
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    dirs = os.listdir(sys.argv[1])
    dirs.remove("outputs")
    
    for dir in dirs:
        process("{0}\\{1}".format(sys.argv[1], dir), output_dir)
    
    
    #process(sys.argv[1])