# Email Forensics XML (EFXML)

Email Forensics XML (EFXML) Schema and Tools

Originally developed as part of the conference paper ["Towards Comprehensive and Collaborative Forensics on Email
Evidence"](https://doi.org/10.4108/icst.collaboratecom.2013.254125), this schema and tool set allow digital forensic
examiners to store email headers as evidence in a format that is tool-agnostic and verifiable.


## License

All code in this project is released under the MIT License. See LICENSE for more details.
